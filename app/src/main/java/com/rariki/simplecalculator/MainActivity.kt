package com.rariki.simplecalculator

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.rariki.simplecalculator.presentation.page.*
import com.rariki.simplecalculator.presentation.view.NumberPad
import com.rariki.simplecalculator.route.Route
import com.rariki.simplecalculator.ui.theme.SimpleCalculatorTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SimpleCalculatorTheme {
                // A surface container using the 'background' color from the theme
                val navController = rememberNavController()
                NavHost(navController = navController, startDestination = "home") {
                    composable("home") {
                        HomePage(
                            navToAddPage = {
                                navController.navigate(Route.add)
                            },
                            navToSubtractPage = {
                                navController.navigate(Route.subtract)
                            },
                            navToMultiplyPage = {
                                navController.navigate(Route.multiply)
                            },
                            navToDividePage = {
                                navController.navigate(Route.divide)
                            },
                            navToSplitEq = {
                                navController.navigate(Route.splitEq)
                            },
                            navToSplitNum = {
                                navController.navigate(Route.splitNum)
                            }
                        )
                    }
                    composable(Route.add) {
                        AddPage()
                    }

                    composable(Route.subtract) {
                        SubtractPage()
                    }

                    composable(Route.multiply) {
                        MultiplyPage()
                    }

                    composable(Route.divide) {
                        DividePage()
                    }

                    composable(Route.splitEq) {
                        SplitEqPage()
                    }

                    composable(Route.splitNum) {
                        SplitNumPage()
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    SimpleCalculatorTheme {
        Greeting("Android")
    }
}