package com.rariki.simplecalculator

import android.app.Application
import com.rariki.simplecalculator.di.SimpleCalculatorModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class SimpleCalculatorApp: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@SimpleCalculatorApp)
            modules(SimpleCalculatorModules.modules)
        }
    }
}