package com.rariki.simplecalculator.presentation.page

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import com.rariki.simplecalculator.presentation.view.BaseContent
import com.rariki.simplecalculator.presentation.viewmodel.SubtractViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun SubtractPage(vm: SubtractViewModel = getViewModel()) {
    val input by vm.input.collectAsState()
    val numbers by vm.numbers.collectAsState()
    val result by vm.result.collectAsState()

    BaseContent(
        input = input,
        numbers = numbers,
        result = result,
        onTyping = {
            vm.changeInput(it.toString())
        },
        onDotClicked = vm::changeInput,
        onDelete = vm::delete,
        onDeleteNumber = vm::deleteNumber,
        onAdd = vm::add,
        onClear = vm::clear,
    )
}