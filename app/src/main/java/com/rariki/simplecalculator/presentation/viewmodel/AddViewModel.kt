package com.rariki.simplecalculator.presentation.viewmodel

import com.rariki.simplecalculator.domain.usecase.AddUseCase

class AddViewModel(
    private val addUseCase: AddUseCase,
) : BaseViewModel() {
    override fun calculateResult(): String {
        return try {
            addUseCase(*numbers.value.toTypedArray()).toString()
        } catch (e: Exception) {
            "0"
        }
    }
}