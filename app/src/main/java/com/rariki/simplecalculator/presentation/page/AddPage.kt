package com.rariki.simplecalculator.presentation.page

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.tooling.preview.Preview
import com.rariki.simplecalculator.presentation.view.BaseContent
import com.rariki.simplecalculator.presentation.viewmodel.AddViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun AddPage(vm: AddViewModel = getViewModel()) {

    val input by vm.input.collectAsState()
    val numbers by vm.numbers.collectAsState()
    val result by vm.result.collectAsState()

    BaseContent(
        input = input,
        numbers = numbers,
        result = result,
        onTyping = {
            vm.changeInput(it.toString())
        },
        onDotClicked = vm::changeInput,
        onDelete = vm::delete,
        onDeleteNumber = vm::deleteNumber,
        onAdd = vm::add,
        onClear = vm::clear,
    )
}


@Preview(showBackground = true)
@Composable
private fun Preview() {
    AddPage()
}