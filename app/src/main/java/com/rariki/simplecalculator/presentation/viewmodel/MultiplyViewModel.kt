package com.rariki.simplecalculator.presentation.viewmodel

import com.rariki.simplecalculator.domain.usecase.MultiplyUseCase

class MultiplyViewModel(
    private val useCase:MultiplyUseCase
): BaseViewModel() {
    override fun calculateResult(): String {
        return try {
            useCase(*numbers.value.toTypedArray()).toString()
        } catch (e: Exception) {
            "0"
        }
    }
}