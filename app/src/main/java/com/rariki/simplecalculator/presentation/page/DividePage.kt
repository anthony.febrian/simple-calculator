package com.rariki.simplecalculator.presentation.page

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import com.rariki.simplecalculator.presentation.view.BaseContent
import com.rariki.simplecalculator.presentation.viewmodel.DivideViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun DividePage(
    vm: DivideViewModel = getViewModel()
) {
    val input by vm.input.collectAsState()
    val numbers by vm.numbers.collectAsState()
    val result by vm.result.collectAsState()
    val enableButtonAdd by vm.enableButtonAdd.collectAsState()

    BaseContent(
        input = input,
        numbers = numbers,
        result = result,
        onTyping = {
            vm.changeInput(it.toString())
        },
        onDotClicked = vm::changeInput,
        onDelete = vm::delete,
        onDeleteNumber = vm::deleteNumber,
        onAdd = vm::add,
        enableButtonAdd = enableButtonAdd,
        onClear = vm::clear,
    )
}