package com.rariki.simplecalculator.presentation.viewmodel

import androidx.lifecycle.viewModelScope
import com.rariki.simplecalculator.domain.usecase.DivideUseCase
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.stateIn

class DivideViewModel(
    private val useCase: DivideUseCase
) : BaseViewModel() {

    val enableButtonAdd = numbers.mapLatest {
        it.size < 2
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(),
        initialValue = true
    )

    override fun calculateResult(): String {
        return try {
            if(numbers.value.size >= 2) {
                val number1 = numbers.value.first()
                val number2 = numbers.value[1]
                useCase(number1, number2).toString()
            } else {
                "0"
            }
        } catch (e: Exception) {
            e.message ?: "0"
        }
    }
}