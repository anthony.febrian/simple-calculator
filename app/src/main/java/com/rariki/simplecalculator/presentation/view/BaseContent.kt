package com.rariki.simplecalculator.presentation.view

import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.Button
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.rariki.simplecalculator.presentation.item.NumberItem
import java.math.BigDecimal

@Composable
fun BaseContent(
    input: String,
    numbers: List<BigDecimal>,
    result: String,
    onTyping: (Int) -> Unit = {},
    onDotClicked: (String) -> Unit = {},
    onDelete: () -> Unit = {},
    onDeleteNumber: (Int) -> Unit = {},
    enableButtonAdd: Boolean = true,
    onAdd: () -> Unit = {},
    onClear: () -> Unit = {},
) {


    Column(
        modifier = Modifier.padding(16.dp)
    ) {
        Column(
        ) {
            val fontSize = 31.sp
            Row(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(
                    text = "Input: ",
                    fontSize = fontSize,
                )
                Text(
                    text = input,
                    fontSize = fontSize,
                    modifier = Modifier
                        .horizontalScroll(rememberScrollState())
                )
            }

            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(
                    text = "Numbers: ",
                    fontSize = fontSize
                )


                val scrollState = rememberLazyListState()
                LazyRow(
                    state = scrollState
                ) {
                    itemsIndexed(numbers) { index, item ->
                        NumberItem(
                            number = item,
                            fontSize = fontSize,
                        ) {
                            onDeleteNumber(index)
                        }
                    }
                }

                LaunchedEffect(key1 = numbers.size) {
                    if (numbers.isNotEmpty()) {
                        scrollState.scrollToItem(numbers.size - 1)
                    }
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(
                    text = "Result : ",
                    fontSize = fontSize
                )
                Text(
                    text = result,
                    fontSize = fontSize,
                    modifier = Modifier
                        .horizontalScroll(rememberScrollState())
                )
            }

            Text(
                "*Note : Result automatically calculated after more than 2 numbers added",
                color = Color.Red,
                fontSize = 11.sp
            )
        }

        Column(
            modifier = Modifier
                .padding(top = 8.dp, bottom = 8.dp)
                .weight(1f)
        ) {
            OutlinedButton(
                onClick = onAdd,
                modifier = Modifier.fillMaxWidth(),
                enabled = enableButtonAdd
            ) {
                Text(text = ("Add"))
            }

            NumberPad(
                onClick = onTyping,
                onDotClicked = onDotClicked,
                onDelete = onDelete,
                onClear = onClear,
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun Preview() {
    BaseContent(
        input = "4",
        numbers = listOf(),
        result = "5"
    )
}