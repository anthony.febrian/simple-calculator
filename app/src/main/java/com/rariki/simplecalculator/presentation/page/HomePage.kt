package com.rariki.simplecalculator.presentation.page

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.airbnb.lottie.compose.*
import com.rariki.simplecalculator.R


@Composable
fun HomePage(
    navToAddPage: () -> Unit = {},
    navToSubtractPage: () -> Unit = {},
    navToMultiplyPage: () -> Unit = {},
    navToDividePage: () -> Unit = {},
    navToSplitEq: () -> Unit = {},
    navToSplitNum: () -> Unit = {},
) {
    Scaffold(
//        topBar = {
//            TopAppBar(
//                title = { Text("Simple Calculator") }
//            )
//        }
    ) { padding ->
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxSize()
        ) {
            val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.calculator))
            val progress by animateLottieCompositionAsState(
                composition,
                iterations = LottieConstants.IterateForever,
            )

            Row(
                verticalAlignment = Alignment.CenterVertically,
            ) {
                LottieAnimation(
                    composition = composition,
                    progress = { progress },
                    modifier = Modifier
                        .size(80.dp)
                )
                
                Text(
                    text = "Simple calculator",
                    fontSize = 21.sp,
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colors.primary,
                    modifier = Modifier
                        .padding(start = 16.dp)
                )
            }

            val borderStroke = BorderStroke(1.dp, MaterialTheme.colors.primary)
            OutlinedButton(
                onClick = navToAddPage,
                border = borderStroke,
                modifier = Modifier
                    .padding(top = 8.dp, bottom = 8.dp)
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                Text(text = "Add")
            }

            OutlinedButton(
                onClick = navToSubtractPage,
                border = borderStroke,
                modifier = Modifier
                    .padding(top = 8.dp, bottom = 8.dp)
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                Text(text = "Subtract")
            }

            OutlinedButton(
                onClick = navToDividePage,
                border = borderStroke,
                modifier = Modifier
                    .padding(top = 8.dp, bottom = 8.dp)
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                Text(text = "Divide")
            }

            OutlinedButton(
                onClick = navToMultiplyPage,
                border = borderStroke,
                modifier = Modifier
                    .padding(top = 8.dp, bottom = 8.dp)
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                Text(text = "Multiply")
            }

            OutlinedButton(
                onClick = navToSplitEq,
                border = borderStroke,
                modifier = Modifier
                    .padding(top = 8.dp, bottom = 8.dp)
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                Text(text = "Split Eq")
            }
            OutlinedButton(
                onClick = navToSplitNum,
                border = borderStroke,
                modifier = Modifier
                    .padding(top = 8.dp, bottom = 8.dp)
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                Text(text = "Split Num")
            }
        }
    }
}

@Composable
@Preview(showBackground = true)
private fun Preview() {
    HomePage()
}