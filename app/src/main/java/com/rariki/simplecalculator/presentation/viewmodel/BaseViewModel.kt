package com.rariki.simplecalculator.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.*
import java.math.BigDecimal

abstract class BaseViewModel : ViewModel() {
    private val _input = MutableStateFlow("")
    val input = _input.asStateFlow()

    private val _numbers = MutableStateFlow<List<BigDecimal>>(listOf())
    val numbers = _numbers.asStateFlow()

    val result = numbers.mapLatest { numbers ->
        calculateResult()
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(),
        initialValue = "0"
    )

    abstract fun calculateResult(): String

    fun changeInput(input: String) {
        val tempInput = this.input.value + input
        Log.d("AddViewModel", tempInput)
        if (input == ".") {
            if (this.input.value.isEmpty() || this.input.value.contains(".")) {
                return
            }
            _input.value = tempInput
        } else if (tempInput.toBigDecimalOrNull() != null) {
            _input.value = tempInput
        }
    }

    fun delete() {
        val tempInput = this.input.value.dropLast(1)
        _input.value = tempInput
    }

    fun deleteNumber(index: Int) {
        val tempNumbers = numbers.value.filterIndexed { i, _ -> i != index }
        _numbers.value = tempNumbers
    }

    fun add() {
        if (input.value.toBigDecimalOrNull() != null) {
            val tempNumbers = arrayListOf<BigDecimal>()
                .apply {
                    addAll(numbers.value)
                    input.value.toBigDecimalOrNull()?.let {
                        add(it)
                        _input.value = ""
                    }
                }
            _numbers.value = tempNumbers
        }
    }

    fun clear() {
        _numbers.value = listOf()
    }
}