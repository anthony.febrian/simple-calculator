package com.rariki.simplecalculator.presentation.view

import androidx.compose.foundation.layout.*
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun NumberPad(
    onClick: (Int) -> Unit = {},
    onDotClicked:(String) -> Unit = {},
    onDelete: () -> Unit = {},
    onClear: () -> Unit = {},
    buttonColor: Color = MaterialTheme.colors.primary,
    textColor: Color = Color.White,
) {
    val padding = 8.dp
    Column(
        modifier = Modifier
            .fillMaxSize()
            .fillMaxWidth()
    ) {
        Row(
            modifier = Modifier
                .weight(1f)
        ) {
            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = { onClick(7) },
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "7",
                    color = textColor
                )
            }

            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = { onClick(8) },
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "8",
                    color = textColor
                )
            }

            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = { onClick(9) },
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "9",
                    color = textColor
                )
            }
        }

        Row(
            modifier = Modifier
                .weight(1f)
        ) {
            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = { onClick(4) },
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "4",
                    color = textColor
                )
            }

            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = { onClick(5) },
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "5",
                    color = textColor
                )
            }

            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = { onClick(6) },
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "6",
                    color = textColor
                )
            }
        }

        Row(
            modifier = Modifier
                .weight(1f)
        ) {
            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = { onClick(1) },
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "1",
                    color = textColor
                )
            }

            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = { onClick(2) },
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "2",
                    color = textColor
                )
            }

            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = { onClick(3) },
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "3",
                    color = textColor
                )
            }
        }

        Row(
            modifier = Modifier
                .weight(1f)
        ) {
            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = { onClick(0) },
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "0",
                    color = textColor
                )
            }

            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = { onDotClicked(".") },
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = ".",
                    color = textColor
                )
            }

            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = onDelete,
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "Del",
                    color = textColor
                )
            }
        }
        
        Row(
            modifier = Modifier
                .weight(1f)
        ) {
            FloatingActionButton(
                backgroundColor = buttonColor,
                onClick = onClear,
                modifier = Modifier
                    .weight(1f)
                    .aspectRatio(1f)
                    .padding(padding)
            ) {
                Text(
                    text = "C",
                    color = textColor
                )
            }
            
            Spacer(modifier = Modifier.weight(2f))
        }
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun Preview() {
    NumberPad()
}