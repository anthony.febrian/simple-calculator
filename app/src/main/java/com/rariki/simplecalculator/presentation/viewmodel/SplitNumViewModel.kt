package com.rariki.simplecalculator.presentation.viewmodel

import com.rariki.simplecalculator.domain.usecase.SplitNumUseCase

class SplitNumViewModel(
    private val useCase:SplitNumUseCase
):BaseViewModel() {
    override fun calculateResult(): String {
        return try {
            useCase(*numbers.value.toTypedArray()).toString()
        } catch (e: Exception) {
            e.message ?: "0"
        }
    }
}