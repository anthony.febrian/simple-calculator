package com.rariki.simplecalculator.presentation.viewmodel

import androidx.lifecycle.viewModelScope
import com.rariki.simplecalculator.domain.usecase.SplitEqUseCase
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.stateIn

class SplitEqViewModel(
    private val useCase:SplitEqUseCase,
):BaseViewModel() {

    val enableButtonAdd = numbers.mapLatest {
        it.size < 2
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(),
        initialValue = true
    )

    override fun calculateResult(): String {
        return try {
            if(numbers.value.size > 1) {
                val number1 = numbers.value.first()
                val number2 = numbers.value[1]

                useCase(number1, number2.toInt()).toString()
            } else "0"
        } catch (e: Exception) {
            e.message ?: "0"
        }
    }
}