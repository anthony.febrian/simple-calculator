package com.rariki.simplecalculator.route

object Route {
    val add = "add"
    val subtract = "subtract"
    val divide = "divide"
    val multiply = "multiply"
    val splitEq = "spliteq"
    val splitNum = "splitnum"
}