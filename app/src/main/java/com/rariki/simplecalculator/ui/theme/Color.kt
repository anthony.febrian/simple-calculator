package com.rariki.simplecalculator.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFF0158F7)
val Purple500 = Color(0xFF0254E9)
val Purple700 = Color(0xFF0049CD)
val Teal200 = Color(0xFF03DAC5)