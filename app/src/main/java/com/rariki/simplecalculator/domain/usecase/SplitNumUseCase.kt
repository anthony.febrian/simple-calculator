package com.rariki.simplecalculator.domain.usecase

import java.math.BigDecimal

interface SplitNumUseCase {
    operator fun invoke(vararg input: BigDecimal): BigDecimal
}