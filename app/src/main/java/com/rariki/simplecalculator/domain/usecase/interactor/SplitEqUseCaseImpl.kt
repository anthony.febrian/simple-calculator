package com.rariki.simplecalculator.domain.usecase.interactor

import com.rariki.simplecalculator.domain.usecase.SplitEqUseCase
import java.math.BigDecimal

class SplitEqUseCaseImpl: SplitEqUseCase {
    override fun invoke(input1: BigDecimal, input2: Int): List<BigDecimal> {
        if (input1 < input2.toBigDecimal()) {
            throw Exception("Second parameter cant more than first parameter")
        }
        return arrayListOf<BigDecimal>().apply {
            val divideResult = input1 / input2.toBigDecimal()

            for (i in 0 until input2) {
                add(divideResult)
            }
        }
    }
}