package com.rariki.simplecalculator.domain.usecase

import java.math.BigDecimal

interface MultiplyUseCase {
    operator fun invoke(vararg input: BigDecimal): BigDecimal
}