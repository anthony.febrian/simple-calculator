package com.rariki.simplecalculator.domain.usecase

import java.math.BigDecimal

interface AddUseCase {
    operator fun invoke(vararg input: BigDecimal): BigDecimal
}