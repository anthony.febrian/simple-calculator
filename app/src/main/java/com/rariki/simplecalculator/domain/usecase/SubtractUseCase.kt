package com.rariki.simplecalculator.domain.usecase

import java.math.BigDecimal

interface SubtractUseCase {
    operator fun invoke(vararg input:BigDecimal):BigDecimal
}