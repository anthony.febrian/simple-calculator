package com.rariki.simplecalculator.domain.usecase.interactor

import com.rariki.simplecalculator.domain.usecase.DivideUseCase
import java.math.BigDecimal
import java.math.RoundingMode

class DivideUseCaseImpl: DivideUseCase {
    override fun invoke(input1: BigDecimal, input2: BigDecimal): BigDecimal {
        if (input2 == 0.toBigDecimal()) {
            throw Exception("Can't divide by 0")
        }
        return input1.divide(input2,1, RoundingMode.HALF_UP)
    }
}