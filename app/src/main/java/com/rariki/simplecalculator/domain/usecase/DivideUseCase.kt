package com.rariki.simplecalculator.domain.usecase

import java.math.BigDecimal

interface DivideUseCase {
    operator fun invoke(input1: BigDecimal, input2: BigDecimal): BigDecimal
}