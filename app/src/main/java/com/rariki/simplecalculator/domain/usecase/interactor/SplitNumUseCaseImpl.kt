package com.rariki.simplecalculator.domain.usecase.interactor

import com.rariki.simplecalculator.domain.usecase.SplitNumUseCase
import com.rariki.simplecalculator.domain.usecase.SubtractUseCase
import java.lang.Exception
import java.math.BigDecimal

class SplitNumUseCaseImpl(
    private val subtractUseCase: SubtractUseCase
): SplitNumUseCase {
    override fun invoke(vararg input: BigDecimal): BigDecimal {
        if (input.size < 2) {
            throw Exception("Input cant be less than 2")
        }
        val value = subtractUseCase(*input)
        if (value <= 0.toBigDecimal()) {
            throw Exception("No remainder")
        }
        return value
    }
}