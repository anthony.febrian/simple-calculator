package com.rariki.simplecalculator.domain.usecase.interactor

import com.rariki.simplecalculator.domain.usecase.AddUseCase
import java.math.BigDecimal

class AddUseCaseImpl:AddUseCase {

    override fun invoke(vararg input: BigDecimal): BigDecimal {
        if (input.size < 2) {
            throw Exception("Input cant be less than 2")
        }
        return input.sumOf { it }
    }
}