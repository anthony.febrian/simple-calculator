package com.rariki.simplecalculator.domain.usecase

import java.math.BigDecimal

interface SplitEqUseCase {
    operator fun invoke(input1: BigDecimal, input2: Int): List<BigDecimal>
}