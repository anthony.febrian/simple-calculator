package com.rariki.simplecalculator.domain.usecase.interactor

import com.rariki.simplecalculator.domain.usecase.SubtractUseCase
import java.lang.Exception
import java.math.BigDecimal

class SubtractUseCaseImpl: SubtractUseCase {
    override fun invoke(vararg input: BigDecimal): BigDecimal {
        if (input.size < 2) {
            throw Exception("Input cant be less than 2")
        }
        return input.reduce { sum, bigDecimal -> sum - bigDecimal }
    }
}