package com.rariki.simplecalculator.di

import com.rariki.simplecalculator.domain.usecase.*
import com.rariki.simplecalculator.domain.usecase.interactor.*
import com.rariki.simplecalculator.presentation.viewmodel.*
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.factoryOf
import org.koin.dsl.bind
import org.koin.dsl.module

object SimpleCalculatorModules {
    val modules = module {
        /**
         * DOMAIN
         */
        factoryOf(::AddUseCaseImpl) bind AddUseCase::class
        factoryOf(::DivideUseCaseImpl) bind DivideUseCase::class
        factoryOf(::MultiplyUseCaseImpl) bind MultiplyUseCase::class
        factoryOf(::SplitEqUseCaseImpl) bind SplitEqUseCase::class
        factoryOf(::SplitNumUseCaseImpl) bind SplitNumUseCase::class
        factoryOf(::SubtractUseCaseImpl) bind SubtractUseCase::class

        /**
         * Presentation
         */
        viewModelOf(::AddViewModel)
        viewModelOf(::DivideViewModel)
        viewModelOf(::MultiplyViewModel)
        viewModelOf(::SplitEqViewModel)
        viewModelOf(::SplitNumViewModel)
        viewModelOf(::SubtractViewModel)
    }
}