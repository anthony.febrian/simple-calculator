package com.rariki.simplecalculator.domain.usecase.interactor

import com.rariki.simplecalculator.domain.usecase.DivideUseCase
import org.junit.Assert
import org.junit.Test


class DivideUseCaseImplTest {
    private val useCase: DivideUseCase = DivideUseCaseImpl()

    @Test
    fun `1 div 1 = 1`() {
        val result = useCase(1.toBigDecimal(), 1.toBigDecimal())
        Assert.assertEquals(1.0.toBigDecimal(), result)
    }

    @Test
    fun `Test Decimal Parameter`() {
        val result = useCase(2.2.toBigDecimal(), 2.toBigDecimal())
        Assert.assertEquals(1.1.toBigDecimal(), result)

    }

    @Test
    fun `Test Negative Parameter`() {
        val result = useCase((-1.1).toBigDecimal(), (-2).toBigDecimal())
        Assert.assertEquals((0.6).toBigDecimal(), result)
    }
}