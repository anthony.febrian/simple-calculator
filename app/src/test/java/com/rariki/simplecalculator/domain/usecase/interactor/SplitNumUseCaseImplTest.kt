package com.rariki.simplecalculator.domain.usecase.interactor

import com.rariki.simplecalculator.domain.usecase.SplitNumUseCase
import com.rariki.simplecalculator.domain.usecase.SubtractUseCase
import org.junit.Assert
import org.junit.Test


class SplitNumUseCaseImplTest {
    private val subtractUseCase: SubtractUseCase = SubtractUseCaseImpl()
    private val useCase: SplitNumUseCase = SplitNumUseCaseImpl(subtractUseCase)

    @Test
    fun `Throw Exception when paramater less than 2`() {
        val exception = Assert.assertThrows(
            Exception::class.java,
        ) {
            useCase(1.toBigDecimal())
        }

        Assert.assertEquals("Input cant be less than 2", exception.message)
    }

    @Test
    fun `140, 45, 35, 20 40`() {
        val result = useCase(
            140.toBigDecimal(), 45.toBigDecimal(), 35.toBigDecimal(), 20.toBigDecimal()
        )

        Assert.assertEquals(40.toBigDecimal(), result)
    }

    @Test
    fun `Throw Error when remainder not found`() {
        val exception = Assert.assertThrows(
            Exception::class.java,
        ) {
            useCase(
                140.toBigDecimal(),
                45.toBigDecimal(),
                35.toBigDecimal(),
                20.toBigDecimal(),
                100.toBigDecimal()
            )
        }

        Assert.assertEquals("No remainder", exception.message)
    }
}