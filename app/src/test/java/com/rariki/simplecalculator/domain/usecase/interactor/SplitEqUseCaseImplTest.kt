package com.rariki.simplecalculator.domain.usecase.interactor

import com.rariki.simplecalculator.domain.usecase.SplitEqUseCase
import org.junit.Assert
import org.junit.Test

class SplitEqUseCaseImplTest {
    private val useCase: SplitEqUseCase = SplitEqUseCaseImpl()

    @Test
    fun `120, 4 {30, 30, 30, 30}`() {
        val result = useCase(120.toBigDecimal(), 4)


        Assert.assertEquals(4, result.size) //check size array
        Assert.assertEquals(30.toBigDecimal(), result.first()) //check value array
    }

    @Test
    fun `150, 2 {75, 75}`() {
        val result = useCase(150.toBigDecimal(), 2)


        Assert.assertEquals(2, result.size) //check size array
        Assert.assertEquals(75.toBigDecimal(), result.first()) //check value array
    }
}