package com.rariki.simplecalculator.domain.usecase.interactor

import com.rariki.simplecalculator.domain.usecase.SubtractUseCase
import org.junit.Assert
import org.junit.Test


class SubtractUseCaseImplTest {
    private val useCase: SubtractUseCase = SubtractUseCaseImpl()

    @Test
    fun `Throw Exception when paramater less than 2`() {
        val exception = Assert.assertThrows(
            Exception::class.java,
        ) {
            useCase(1.toBigDecimal())
        }

        Assert.assertEquals(exception.message, "Input cant be less than 2")
    }

    @Test
    fun `1 - 1 = 0`() {
        val result = useCase(1.toBigDecimal(), 1.toBigDecimal())
        Assert.assertEquals(0.toBigDecimal(), result)
    }

    @Test
    fun `Test Decimal Parameter`() {
        val result = useCase(2.1.toBigDecimal(), 2.toBigDecimal())
        Assert.assertEquals(0.1.toBigDecimal(), result)

    }

    @Test
    fun `Test Negative Parameter`() {
        val result = useCase((-1.1).toBigDecimal(), (-2.0).toBigDecimal())
        Assert.assertEquals((0.9).toBigDecimal(), result)
    }
}