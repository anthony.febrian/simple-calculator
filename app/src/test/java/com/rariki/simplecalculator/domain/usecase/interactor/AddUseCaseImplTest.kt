package com.rariki.simplecalculator.domain.usecase.interactor

import com.rariki.simplecalculator.domain.usecase.AddUseCase
import org.junit.Assert.*
import org.junit.Test


class AddUseCaseImplTest {
    private val useCase: AddUseCase = AddUseCaseImpl()

    @Test
    fun `Throw Exception when paramater less than 2`() {
        val exception = assertThrows(
            Exception::class.java,
        ) {
            useCase(1.toBigDecimal())
        }

        assertEquals("Input cant be less than 2", exception.message)
    }

    @Test
    fun `1 + 1 = 2`() {
        val result = useCase(1.toBigDecimal(), 1.toBigDecimal())
        assertEquals(2.toBigDecimal(), result)
    }

    @Test
    fun `Test Decimal Parameter`() {
        val result = useCase(1.1.toBigDecimal(), 2.toBigDecimal())
        assertEquals(3.1.toBigDecimal(), result)
    }

    @Test
    fun `Test Negative Parameter`() {
        val result = useCase(1.1.toBigDecimal(), (-2.0).toBigDecimal())
        assertEquals((-0.9).toBigDecimal(), result)
    }
}